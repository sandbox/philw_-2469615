INTRODUCTION
------------
This module adds an ARIA landmark role and aria-label attributes to the default
Drupal messages output for errors, warnings, and status messages. These
attributes help prompt assistive technology (AT) such as screen readers to
announce these errors on the page load (or introduction of the error to the DOM
for messages introduced by AJAX). This announcement serves as an equivalent of
the message being prominent to sighted users through colouring and position at
the top of the page.

The module backports some of the accessibility work done by jessebeach which is
part of the core messages template in Drupal 8.

 * For a full description of the module, visit the sandbox page:
   https://www.drupal.org/sandbox/philw_/2469615

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2469615


CHANGES TO THE DEFAULT OUTPUT
-----------------------------
  1.  The messages container div now has an ARIA landmark set:
      role="contentinfo" as well as an appropriate label set, such as:
      aria-label="Error message".

  2.  Error or status messages are wrapped in an additional div and given an
      appropriate ARIA role attribute such as role="alert" or role="status"

INSTALLATION
------------
  * Install as you would normally install a contributed Drupal module. See:
    https://drupal.org/documentation/install/modules-themes/modules-7
    for further information.

CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will affect the output of the messages displayed by Drupal.

TROUBLESHOOTING
---------------
  * This module will override any changes that your theme tries to make to the
    display of messages in Drupal. If your theme calls theme_status_messages()
    then you may wish to merge the the changes this module makes in
    aria_messages_status_messages() with your theme's theme_status_messages()
    function.
